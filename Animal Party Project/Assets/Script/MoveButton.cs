﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveButton : MyButton {

    public Transform MovePos;    
    Collider SelfCollider;
    string TAG = "[MoveButton] -- ";

	// Use this for initialization
	public override void Start () {

        //Debug.Log(TAG + "Is SteamVR Actice? : " + SteamVR.active);
        //Debug.Log(TAG + "Is SteamVR Actice? : " + GameObject.Find("[CameraRig]").name);

        SelfCollider = GetComponent<Collider>();


        if (!Interaction.VRMode)
        {
            GO = Camera.main.gameObject;
        }
        else
        {
            GO = GameObject.Find("[CameraRig]");
        }

        OColor = GetComponent<Renderer>().material.color;

    }

    public void OnTriggerEnter(Collider other)
    {
        SelfCollider.enabled = false;
        StartCoroutine(MoveVIVE(GO, MovePos));    
    }

    IEnumerator MoveVIVE(GameObject go , Transform destination)
    {
        //畫面變黑
        SteamVR_Fade.Start(Interaction.FadeColor, Interaction.FadeTime);

        //等待畫面變黑
        yield return new WaitForSeconds(Interaction.FadeTime);

        //移動玩家
        Interaction.MoveUser(GO, MovePos);

        yield return new WaitForSeconds(Interaction.FadeTime);


        //畫面恢復
        SelfCollider.enabled = true;
        SteamVR_Fade.Start(new Color(0.0f, 0.0f, 0.0f, 0.0f), Interaction.FadeTime);
       
    }

    IEnumerator MovePC(GameObject go, Transform destination)
    {
        //畫面變黑
        //Debug.Log("開始變黑");

        //等待畫面變黑
        yield return new WaitForSeconds(Interaction.FadeTime);

        //移動玩家
        //Debug.Log("移動");
        Interaction.MoveUser(GO, MovePos);

        yield return new WaitForSeconds(Interaction.FadeTime);


        //畫面恢復
        SelfCollider.enabled = true;
        //Debug.Log("開始恢復");

    }

}
