﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractionObject : MonoBehaviour {

    Vector3 OPos;
    public Vector3 OForward;
    Vector3 OScale;
    public float maxscale = 2.0f;

    // Use this for initialization
    void Start () {
        OPos = gameObject.transform.position;
        OScale = gameObject.transform.localScale;
        OForward = gameObject.transform.forward;
    }

    public void ResetObject()
    {
        //gameObject.transform.position = OPos;
        //gameObject.transform.localScale = OScale;
        //gameObject.transform.forward = OForward;

        transform.DOScale(OScale, 1.0f);
        transform.DOLookAt(OForward, 1.0f);
    }
}
