﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ResetButton : MyButton {

    public  MyButton BigButton;

    public void OnTriggerEnter(Collider other)
    {
        BigButton.ResetColor();

        GO.GetComponent<InteractionObject>().ResetObject();

    }

}
