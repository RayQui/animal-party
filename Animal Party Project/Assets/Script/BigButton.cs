﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigButton : MyButton {

    public Color DesColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);

    public void OnTriggerStay(Collider other)
    {
        if (Interaction.ScaleObject(GO, IO.maxscale))
            GetComponent<Renderer>().material.color = DesColor;
        
    }
}
