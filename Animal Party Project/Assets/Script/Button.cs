﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyButton :MonoBehaviour  {

    [HideInInspector]
    public GameObject GO;
    [HideInInspector]
    public InteractionObject IO;
    [HideInInspector]
    public Color OColor;

    public virtual void Start()
    {
        IO = transform.parent.GetComponentInChildren<InteractionObject>();
        GO = IO.gameObject;
        OColor = GetComponent<Renderer>().material.color;
    }

    public void ResetColor()
    {
        GetComponent<Renderer>().material.color = OColor;
    }

    public void Switch(bool OnOff)
    {
        gameObject.SetActive(OnOff);
    }
}
