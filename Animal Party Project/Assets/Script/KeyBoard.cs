﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class KeyBoard : MonoBehaviour {

    string TAG = "[KeyBoard]--";
    public GameObject TargetObject;
    public InteractionObject IO;

    // Use this for initialization
    void Start () {
        IO = TargetObject.GetComponent<InteractionObject>();
        if (IO == null)
            Debug.LogError(TAG + "物件 : " + TargetObject.name + "並沒有IntertationObject腳本");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.B))
        {
            Interaction.ScaleObject(TargetObject,IO.maxscale);
        }
        if (Input.GetKey(KeyCode.S))
        {
            Interaction.UnScaleObject(TargetObject);
        }


        if (Input.GetKeyDown(KeyCode.R))
        {
            TargetObject.transform.DOScale(Vector3.one, 1.0f);
        }

        if (Input.GetKey(KeyCode.R))
        {
            Interaction.RotateObjectByTouch(TargetObject);
        }

        if (Input.GetKey(KeyCode.P))
        {
            Interaction.PlayAnimate(TargetObject);
        }
        //if (Input.GetKey(KeyCode.R))
        //{
        //    Interaction.RestObject(TargetObject);
        //}
    }
}
