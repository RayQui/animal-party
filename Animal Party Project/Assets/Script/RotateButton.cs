﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateButton : MyButton
{

    GameObject Hand;

    //當旋轉功能開關時，會被開關的按鈕
    public MyButton[] ControllButton;
    //當旋轉功能開關時，會被開關的Sprite
    public GameObject[] ControllSprite;

    float RoateRate = 360.0f;
    float CurrentRotateY = 0.0f;
    float OldXPos;

    private void Update()
    {
        if (Hand != null)
        {
            CurrentRotateY += FindXMove(Hand);
            Interaction.RotateObjectUseHand(GO, CurrentRotateY);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        GO.GetComponent<InteractionObject>().ResetObject();

        GameObject tmpgo = other.gameObject;

        if (Hand == null)
        {
            Hand = tmpgo;
            OldXPos = tmpgo.transform.position.x;
            _switchbutton(false);
        }
        else
        {
            Hand = null;
            CurrentRotateY = 0.0f;
            _switchbutton(true);
        }
    }

    public float FindXMove(GameObject go)
    {
        float currentx = go.transform.position.x;
        float tmp = currentx - OldXPos;
        OldXPos = currentx;
        return tmp * RoateRate;
    }

    void _switchbutton(bool OnOff)
    {
        foreach (MyButton b in ControllButton)
        {
            b.Switch(OnOff);
            b.ResetColor();
            SwitchSprite(!OnOff);
        }
    }

    public void SwitchSprite(bool OnOff)
    {
        foreach (GameObject g in ControllSprite)
        {
            g.SetActive(OnOff);
        }
    }

    //public void OnTriggerStay(Collider other)
    //{
    //    Interaction.RotateObject(GO);
    //}
}
