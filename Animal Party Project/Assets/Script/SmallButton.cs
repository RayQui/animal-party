﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallButton : MyButton {

    public void OnTriggerStay(Collider other)
    {
        Interaction.UnScaleObject(GO);
    }
}
