﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    static public bool VRMode = false;
    public Transform UserIniPos;

    public const float DefaulHigh = 1.7f;
    public const float RotateRate = 1f;
    public const float ScaleRate = 0.5f;
    public const float minscale = 1.0f;
    static public Color FadeColor = new Color(0.0f,0.0f,0.0f,1.0f);
    static public float FadeTime = 0.5f;

    void Awake()
    {
        if (SteamVR.active)
        {
            VRMode = true;
        }
    }

    void Start()
    {

        Debug.Log("FadeColor" + FadeColor + " FadeTime : " + FadeTime);
        if (VRMode)
        {
            MoveUser(GameObject.Find("[CameraRig]"), UserIniPos);
        }
        else
        {
            MoveUser(Camera.main.gameObject, UserIniPos);
        }
    }


    static public bool ScaleObject(GameObject go, float maxscale)
    {
        Vector3 gos = go.transform.localScale;
        if (gos.x >= maxscale)
        {
            gos = Vector3.one * maxscale;
            return true;
        }
        gos.x += ScaleRate * Time.deltaTime;
        gos.y = gos.x;
        gos.z = gos.x;
        go.transform.localScale = gos;
        return false;
    }

    static public void UnScaleObject(GameObject go)
    {
        Vector3 gos = go.transform.localScale;
        if (gos.x <= minscale)
        {
            gos = Vector3.one * minscale;
            return;
        }
        gos.x -= ScaleRate * Time.deltaTime;
        gos.y = gos.x;
        gos.z = gos.x;
        go.transform.localScale = gos;
    }

    static public void RestObject(GameObject go)
    {
        InteractionObject io = go.GetComponent<InteractionObject>();
        if (io != null)
            io.ResetObject();
    }

    /// <summary>
    /// 持續碰觸旋轉
    /// </summary>
    /// <param name="go"></param>
    static public void RotateObjectByTouch(GameObject go)
    {
        go.transform.Rotate(Vector3.up, RotateRate);
    }

    static public void RotateObjectUseHand(GameObject go, float rotaterate)
    {
        go.transform.rotation = Quaternion.Euler(0.0f, rotaterate, 0.0f);
    }

    static public void PlayAnimate(GameObject go)
    {
        Animator goa = go.GetComponent<Animator>();
        //if (goa.enabled)
        //return;

        //goa.enabled = true;
        goa.SetTrigger("Play");
    }

    static public void MoveUser(GameObject go, Transform destination)
    {
        if (VRMode)
        {
            go.transform.position = destination.position;
        }
        else
        {
            go.transform.position = destination.position + new Vector3(0.0f, DefaulHigh, 0.0f);
        }

        go.transform.rotation = destination.rotation;

    }

}
